package com.example.practical23

import android.annotation.SuppressLint
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v4.media.session.MediaSessionCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class MusicNotification {
    companion object{
        val CHANNEL_ID = "channel1"
        val ACTION_PREVIOUS = "actionprevious"
        val ACTION_PLAY = "actionplay"
        val ACTION_NEXT = "actionnext"

        var notification: Notification? = null

        @SuppressLint("UnspecifiedImmutableFlag")
        fun createNotification(
            context: Context,
            tracks: Tracks,
            playButton: Int,
            position: Int,
            size: Int
        ) {
            val sdk_int = Build.VERSION.SDK_INT
            val version_codes = Build.VERSION_CODES.O
            if (sdk_int >= version_codes) {
                val notificationManagerCompat = NotificationManagerCompat.from(context)
                val mediaSessionCompat = MediaSessionCompat(context, "tag")
                val icon = BitmapFactory.decodeResource(context.resources, tracks.getImage())

                //Building Notification Actions View according to song lists
                //For Previous Button
                val pendingIntentPrevious: PendingIntent?
                val drw_previous: Int
                if (position < 0) {
                    pendingIntentPrevious = null
                    drw_previous = 0
                } else {
                    val intentPrevious = Intent(context, NotificationActionService::class.java)
                        .setAction(ACTION_PREVIOUS)
                    pendingIntentPrevious = PendingIntent.getBroadcast(
                        context,
                        0,
                        intentPrevious,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    )
                    drw_previous = R.drawable.ic_baseline_skip_previous_24
                }

                //For Play/Pause Button
                val intentPlay = Intent(context, NotificationActionService::class.java)
                    .setAction(ACTION_PLAY)
                val pendingIntentPlay = PendingIntent.getBroadcast(
                    context,
                    0,
                    intentPlay,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )

                //For Next Button
                val pendingIntentNext: PendingIntent?
                val drw_next: Int
                if (position > 3) {
                    pendingIntentNext = null
                    drw_next = 0
                } else {
                    val intentNext = Intent(context, NotificationActionService::class.java)
                        .setAction(ACTION_NEXT)
                    pendingIntentNext = PendingIntent.getBroadcast(
                        context,
                        0,
                        intentNext,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    )
                    drw_next = R.drawable.ic_baseline_skip_next_24
                }

                //Building Notification
                notification = NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_baseline_library_music_24)
                    .setContentTitle(tracks.getTitle())
                    .setContentText(tracks.getArtist())
                    .setLargeIcon(icon)
                    .setOnlyAlertOnce(true)
                    .setShowWhen(false)
                    .addAction(
                        drw_previous,
                        context.getString(R.string.previous),
                        pendingIntentPrevious
                    )
                    .addAction(playButton, context.getString(R.string.play), pendingIntentPlay)
                    .addAction(drw_next, context.getString(R.string.next), pendingIntentNext)
                    .setStyle(androidx.media.app.NotificationCompat.MediaStyle())
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .build()
                notificationManagerCompat.notify(1, notification!!)
            }
        }
    }


}