package com.example.practical23

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.practical23.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity(), Playable {

    lateinit var binding: ActivityMainBinding
    var mediaPlayer: MediaPlayer? = null
    var notificationManager: NotificationManager? = null
    var tracksList: MutableList<Tracks>? = null
    var position = 0
    var isPlaying = false
    var handler: Handler? = null
    var runnable: Runnable? = null
    var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.extras!!.getString(context.getString(R.string.action_name))
            when (action) {
                MusicNotification.ACTION_PREVIOUS -> onTrackPrevious()
                MusicNotification.ACTION_PLAY -> if (isPlaying) {
                    onTrackPause()
                } else {
                    onTrackPlay()
                }
                MusicNotification.ACTION_NEXT -> onTrackNext()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        handler = Handler()
        binding.seekBar.progress = 0
        binding.ivPrev.visibility = View.INVISIBLE
        binding.ivNext.visibility = View.INVISIBLE

        //Pause/Play Button Check
        if (isPlaying) {
            binding.ivAction.setImageResource(R.drawable.ic_baseline_pause_24)
        } else {
            binding.ivAction.setImageResource(R.drawable.ic_baseline_play_arrow_24)
        }

        //Music Total Time Textview
        binding.txtEndTime.visibility = View.INVISIBLE

        //Load tracks
        prepareTracks()

        //Create Notification Channel and Start Service and BroadcastReceiver
        createNotificationChannel()

        //On Play/Pause Click Event
        binding.ivAction.setOnClickListener {
            binding.ivPrev.visibility = View.VISIBLE
            binding.ivNext.visibility = View.VISIBLE
            MusicNotification.createNotification(
                applicationContext,
                tracksList!![1], R.drawable.ic_baseline_pause_24, 1, tracksList!!.size - 1
            )
            if (isPlaying) {
                onTrackPause()
            } else {
                onTrackPlay()
            }
        }

        //On Previous Song Click Event
        binding.ivPrev.setOnClickListener { view ->
            if (position > 0) {
                onTrackPrevious()
            } else {
                position = 2
                onTrackNext()
            }
        }

        //On Next Song Click Event
        binding.ivNext.setOnClickListener { view ->
            if (position < 3) {
                onTrackNext()
            } else {
                position = 1
                onTrackPrevious()
            }
        }

        //Seek Bar Seek Change Listner for Media Player Seek Control
        binding.seekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {
                mediaPlayer?.seekTo(seekBar.progress)
            }
        })


    }

    private fun prepareTracks() {
        tracksList = ArrayList<Tracks>()
        (tracksList as ArrayList<Tracks>).add(
            Tracks(
                getString(R.string.mere_yaara),
                getString(R.string.arijit_singh),
                R.drawable.t1,
                R.raw.mere_yaara
            )
        )
        (tracksList as ArrayList<Tracks>).add(
            Tracks(
                getString(R.string.raataan),
                getString(R.string.jubin_nautiyal),
                R.drawable.t2,
                R.raw.raatan_lambiyan
            )
        )
        (tracksList as ArrayList<Tracks>).add(
            Tracks(
                getString(R.string.dil_ko_karaar),
                getString(R.string.arijit_singh),
                R.drawable.t3,
                R.raw.dil_ko_karaar
            )
        )
        (tracksList as ArrayList<Tracks>).add(
            Tracks(
                getString(R.string.lagdi_lahore),
                getString(R.string.guru_randhawa),
                R.drawable.t4,
                R.raw.lagdi_lahore_di_aa
            )
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val sdk_int = Build.VERSION.SDK_INT
        val version_codes = Build.VERSION_CODES.O
        if (sdk_int >= version_codes) {
            registerReceiver(broadcastReceiver, IntentFilter(getString(R.string.intent_filter_action)))
            startService(Intent(baseContext, OnClearFromRecent::class.java))
            val notificationChannel = NotificationChannel(
                MusicNotification.CHANNEL_ID,
                getString(R.string.channel_name),
                NotificationManager.IMPORTANCE_LOW
            )
            notificationManager = getSystemService(NotificationManager::class.java)
            if (notificationManager != null) {
                notificationManager?.createNotificationChannel(notificationChannel)
            }
        }
    }

    //Implemented Methods----Notification Actions Control/Background Music Control
    override fun onTrackPrevious() {
        mediaPlayer?.reset()
        position--
        if (position < 0) {
            position = 3
        }
        MusicNotification.createNotification(
            applicationContext, tracksList!![position],
            R.drawable.ic_baseline_pause_24, position, tracksList!!.size - 1
        )
        binding.musicImage.setImageResource(tracksList!![position].getImage())
        loadMusic() //Method call for loading music according to position
    }

    override fun onTrackPlay() {
        MusicNotification.createNotification(
            applicationContext, tracksList!![position],
            R.drawable.ic_baseline_pause_24, position, tracksList!!.size - 1
        )
        isPlaying = true
        binding.musicImage.setImageResource(tracksList!![position].getImage())
        if (mediaPlayer != null) {
            mediaPlayer?.seekTo(mediaPlayer!!.currentPosition)
            mediaPlayer?.start()
        } else {
            loadMusic() //Method call for loading music according to position
        }
        binding.ivAction.setImageResource(R.drawable.ic_baseline_pause_24)
    }

    override fun onTrackPause() {
        MusicNotification.createNotification(
            applicationContext, tracksList!![position],
            R.drawable.ic_baseline_play_arrow_24, position, tracksList!!.size - 1
        )
        isPlaying = false
        binding.ivAction.setImageResource(R.drawable.ic_baseline_play_arrow_24)
        if (mediaPlayer != null) {
            mediaPlayer?.pause()
        }
        binding.musicImage.setImageResource(tracksList!![position].getImage())
    }

    override fun onTrackNext() {
        mediaPlayer?.reset()
        position++
        if (position > 3) {
            position = 0
        }
        MusicNotification.createNotification(
            applicationContext, tracksList!![position],
            R.drawable.ic_baseline_pause_24, position, tracksList!!.size - 1
        )
        binding.musicImage.setImageResource(tracksList!![position].getImage())
        loadMusic() //Method call for loading music according to position
    }

    private fun loadMusic() {
        binding.txtEndTime.visibility = View.VISIBLE
        mediaPlayer = MediaPlayer.create(this, tracksList!![position].getSong_raw())
        with(mediaPlayer) {
            this?.setLooping(false)
            this?.setVolume(100f, 100f)
        }
        binding.txtEndTime.text = ("0:"+this.mediaPlayer!!.getDuration() / 1000).toString()
        this.mediaPlayer?.start()
        binding.seekBar.max = this.mediaPlayer!!.getDuration()
        runnable = Runnable {
            if (mediaPlayer != null) {
                binding.seekBar.progress = mediaPlayer!!.currentPosition
                binding.txtTime.text = ("0:"+mediaPlayer!!.currentPosition / 1000).toString()
            }
            handler?.postDelayed(runnable!!, 50)
        }
        handler?.postDelayed(runnable!!, 50)
    }

    override fun onDestroy() {
        super.onDestroy()
        val sdk_int = Build.VERSION.SDK_INT
        val version_codes = Build.VERSION_CODES.O
        if (sdk_int >= version_codes) {
            notificationManager!!.cancelAll()
        }
        unregisterReceiver(broadcastReceiver)
    }
}